if v:version > 580
  highlight clear
  if exists('syntax_on')
    syntax reset
  endif
endif

set background=dark

let s:keyword = ['#BAACFF', 141]
let s:idenifier = ['#C8D3F5', 252]
let s:string = ['#7AF8CA', 122]
let s:number = ['#FF9668', 216]
let s:label = ['#7FDAFF', 117]
let s:constant = ['#E4F3FA', 254]
let s:comment = ['#7E8EDA', 153]
let s:function = ['#70B0FF', 75]
let s:static_function = ['#70B0FF', 75]
let s:function_call = ['#FFC66D', 221]
let s:static_function_call = ['#70B0FF', 75]
let s:variable = ['#E4F3FA', 254]
let s:class = ['#FFDB8E', 222]
let s:interface = ['#7AF8CA', 157]
let s:field = ['#34D3FB', 75]
let s:todo = ['#B4F9F8', 158]
let s:background = ['#222436', 235]
let s:background_selected = ['#2F334D', 240]
let s:none = ["NONE", "NONE"]

function! s:h(scope, fg, bg)
  let l:hl_string = [
    \ 'highlight!', a:scope,
    \ 'guifg=' . a:fg[0], 'ctermfg=' . a:fg[1],
    \ 'guibg=' . a:bg[0], 'ctermbg=' . a:bg[1],
    \ 'gui=NONE',
    \ 'guisp=NONE'
		\ ]

	execute join(l:hl_string, ' ')
endfunction

call s:h("PreProc", s:idenifier, s:background)
call s:h("Type", s:class, s:background)
call s:h("Identifier", s:idenifier, s:background)
call s:h("Function", s:function, s:background)
call s:h("Constant", s:constant, s:background)
call s:h("Keyword", s:keyword, s:background)
call s:h("Comment", s:comment, s:background)
call s:h("String", s:string, s:background)
call s:h("Number", s:number, s:background)
call s:h("Normal", s:idenifier, s:background)
call s:h("Macro", s:function, s:background)
call s:h("Modifier", s:idenifier, s:background)
call s:h("Operator", s:keyword, s:background)
call s:h("Boolean", s:keyword, s:background)
call s:h("Statement", s:keyword, s:background)
call s:h("LineNr", s:background_selected, s:background)
call s:h("Charactor", s:string, s:background)
call s:h("Todo", s:todo, s:background)
call s:h("Float", s:number, s:background)
call s:h("Conditional", s:keyword, s:background)
call s:h("Label", s:label, s:background)
call s:h("Exception", s:keyword, s:background)
call s:h("Typedef", s:class, s:background)
call s:h("Structure", s:class, s:background)
call s:h("Special", s:keyword, s:background)
call s:h("SpecialComment", s:keyword, s:background)
call s:h("StorageClass", s:keyword, s:background)
call s:h("NonText", s:background, s:none)

call s:h("Visual", s:idenifier, s:background_selected)
call s:h("VisalNOS", s:idenifier, s:background_selected)

call s:h("Pmenu", s:none, s:background)
call s:h("PmenuSbar", s:none, s:background)
call s:h("PmenuSel", s:none, s:background_selected)
call s:h("PmenuThumb", s:none, s:background_selected)

call s:h("VertSplit", s:none, s:background)
call s:h("Pmenu", s:none, s:background)

" if CoC is installed
" TODO: Use link instead

call s:h("CocSem_namespace", s:idenifier, s:background)
call s:h("CocSem_class", s:class, s:background)
call s:h("CocSem_enum", s:class, s:background)
call s:h("CocSem_typeParameter", s:idenifier, s:background)
call s:h("CocSem_event", s:keyword, s:background)
call s:h("CocSem_method", s:function, s:background)
call s:h("CocSem_macro", s:function, s:background)
call s:h("CocSem_keyword", s:keyword, s:background)
call s:h("CocSem_modifier", s:keyword, s:background)
call s:h("CocSem_type", s:class, s:background)
call s:h("CocSem_typeAlias", s:class, s:background)
call s:h("CocSem_struct", s:class, s:background)
call s:h("CocSem_interface", s:interface, s:background)
call s:h("CocSem_selfKeyword", s:keyword, s:background)
call s:h("CocSem_property", s:field, s:background)
call s:h("CocSem_variable", s:idenifier, s:background)
call s:h("CocSem_builtinType", s:keyword, s:background)
call s:h("CocSem_enumMember", s:field, s:background)
call s:h("CocSem_number", s:number, s:background)

" Rust (WIP)
call s:h("rustFuncCall", s:function_call, s:background)
call s:h("rustEnumVarient", s:field, s:background)
call s:h('rustDerive', s:function, s:background)
call s:h('rustAttribute', s:function, s:background)
call s:h('rustDeriveTrait', s:interface, s:background)
