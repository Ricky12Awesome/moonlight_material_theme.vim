# moonlight_material_theme.vim

A Vim theme based on Material Theme UI Moonlight Theme

This is my first vim related project, basically I'm trying to mimic `Moonlight` from 
[Material Theme UI Lite](https://plugins.jetbrains.com/plugin/12124-material-theme-ui-lite) i
from JetBrains IDEs since there is no good one that already exists.

The goal of this theme is to provide good semantic highlighting for a wide range of languages 
for this you'll need [coc.nvim](https://github.com/neoclide/coc.nvim)
